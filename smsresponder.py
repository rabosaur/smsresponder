#!/usr/bin/env python
# pip install boto3
# pip install --upgrade --user awscli
# http://docs.aws.amazon.com/cli/latest/userguide/cli-install-macos.html#awscli-install-osx-path
# http://docs.aws.amazon.com/cli/latest/userguide/installing.html
import imaplib
import boto3

# ref: https://stackoverflow.com/questions/40802200/example-script-to-send-sms-via-aws-sns-using-boto
# see
# ref: https://stackoverflow.com/questions/13210737/get-only-new-emails-imaplib-and-python


save_file = "cur_count.txt"
numbers = ['+14254185162', '+14252398346','+12069313665']
testing = True

def send_sms(message, phone_number):
    testing = False
    log_msg("send_sms({},{})".format(message, phone_number))
    sns_client = boto3.client('sns')
    response = sns_client.publish(
            PhoneNumber=phone_number,
            Message=message,
    )
    log_msg(response)


def log_msg(text):
    with open('sms.log','wb+') as f:
        f.writelines("{}\n".format(text))


def save_count(count):
    with open(save_file, "wb") as f:
        f.write("{}\n".format(count))


def load_count():
    try:
        with open(save_file, "rb") as f:
            count = f.read()
        return int(count)
    except:
        return 0


def check_for_new_emails():
    try:
        obj = imaplib.IMAP4_SSL('imap.gmail.com') # , 993)
        obj.login('schedule@coffeezombie.com', 'lets-morphine-moisture-marigold-ledger')
        current_count = int(obj.select('Inbox')[1][0]) # <-- it will return total number of mail in Inbox i.e ('OK', ['50'])
        return current_count
    except:
        return 0


def main():
    last_count = load_count()
    current_count = check_for_new_emails()
    if last_count < current_count:
        print "time to ping SMS through SNS"
        for number in numbers:
        	send_sms("new email to schedule@coffeezombie.com", number) 
	save_count(current_count)
    else:
        print "no pinging SMS"


main()
